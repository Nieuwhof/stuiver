![rsz_15-cents-vierkante-stuiver](https://gitlab.com/Nieuwhof/stuiver/-/raw/main/stuiver.png) 

<img src="https://coincodex.com/en/resources/images//static/media/cc-logo-png.png" width=20% >   Listed as https://coincodex.com/crypto/stuiver/

Please use the following Stuiver LOGO - https://gitlab.com/Nieuwhof/stuiver/-/blob/main/stuiver.png

# Services
1. Stuiver Paperwallet creator - http://stuiverpaperwallet.ga/
2. Stuiver Safe Cold paperwallet creator - Download and run from your PC disconnected from internet. https://gitlab.com/Nieuwhof/stuiver/-/blob/main/Download.zip
3. Stuiver mining pool : 
            - Mining4people.com - https://mining4people.com

4. Script to make your own cryptocoin and docker miners : https://gitlab.com/Nieuwhof/stuiver/-/blob/main/script.sh
5. Windows Wallets - https://github.com/Nieuwhof/stuiver/blob/main/stuiver-window-wallets.zip - Then Press Download.
6. more stuiver services soon .....

We need:
1. a web wallet - Could somebody assist
2. Need help in wording and people helping with Stuiver development in general - will pay on offer
3. Somebody get Stuiver on a exchange
4. More good-looking logo size .ico,16,32,64,128 
5. Offer help and you get a big thanks


Thank you very much for your help and understanding

You are great !!

 Personal Contact:
 -----------------
 
 - `the.stuiver @gmail.com`  (Space has been inserted infront of @gmail to prevent SPAM - remove it when sending mail)
 - Twitter - @theStuiver

 Mining example
 --------------
 cgminer -o stratum+tcp://<ip?>:3433 -u stuiveraddress -p c=password (* out of service please do not use *)
 
 
 # Forum
 https://bitcointalk.org/index.php?topic=5395497.msg59925102#msg59925102
 

Stuiver Core integration/staging tree
=====================================

What is Stuiver?
----------------

Stuiver is an experimental digital currency that enables instant payments to
anyone, anywhere in the world. Stuiver uses peer-to-peer technology to operate
with no central authority: managing transactions and issuing money are carried
out collectively by the network. Stuiver Core is the name of open source
software which enables the use of this currency.

For more information, as well as an immediately useable, binary version of
the StuiverCore software,

License
-------

Stuiver Core is released under the terms of the MIT license. See [COPYING](COPYING) for more
information or see https://opensource.org/licenses/MIT.

Development Process
-------------------

The `master` branch is regularly built and tested, but is not guaranteed to be
completely stable. 
regularly to indicate new official, stable release versions of Stuiver Core.

The contribution workflow is described in [CONTRIBUTING.md](CONTRIBUTING.md).

The developer should be used to discuss complicated or controversial changes before working
on a patch set.

Testing
-------

Testing and code review is the bottleneck for development; we get more pull
requests than we can review and test on short notice. Please be patient and help out by testing
other people's pull requests, and remember this is a security-critical project where any mistake might cost people
lots of money.

### Automated Testing

Developers are strongly encouraged to write [unit tests](src/test/README.md) for new code, and to
submit new unit tests for old code. Unit tests can be compiled and run
(assuming they weren't disabled in configure) with: `make check`. Further details on running
and extending unit tests can be found in [/src/test/README.md](/src/test/README.md).

There are also [regression and integration tests](/test), written
in Python, that are run automatically on the build server.
These tests can be run (if the [test dependencies](/test) are installed) with: `test/functional/test_runner.py`

The Travis CI system makes sure that every pull request is built for Windows, Linux, and OS X, and that unit/sanity tests are run automatically.

### Manual Quality Assurance (QA) Testing

Changes should be tested by somebody other than the developer who wrote the
code. This is especially important for large or high-risk changes. It is useful
to add a test plan to the pull request description if testing the changes is
not straightforward.

Translations
------------

We only accept translation fixes that are submitted through [Bitcoin Core's Transifex page](https://www.transifex.com/projects/p/bitcoin/).
Translations are converted to Stuiver periodically.

Translations are periodically pulled from Transifex and merged into the git repository. See the
[translation process](doc/translation_process.md) for details on how this works.

**Important**: We do not accept translation changes as GitHub pull requests because the next
pull from Transifex would automatically overwrite them again.


# Modern Build instructions

Debian

These build instructions should work for Debian and derivative systems, such as Ubuntu and Linux Mint.

We have some required packages. You can install these with apt:

{apt install qt5-qmake qt5-default qttools5-dev-tools \
    libboost-system-dev libboost-filesystem-dev \
    libboost-program-options-dev libboost-thread-dev \
    build-essential libboost-dev \
    libssl-dev libdb5.3++-dev libminiupnpc-dev}
    
Next build the wallet:

qmake
make


# Compiling server extras

echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf > /dev/null

apt-get install apt-transport-https

sudo apt-get install software-properties-common

sudo add-apt-repository ppa:bitcoin/bitcoin

sudo apt-get update

apt-get -y install ccache git libboost-system1.58.0 libboost-filesystem1.58.0 libboost-program-options1.58.0 libboost-thread1.58.0 libboost-chrono1.58.0 libssl1.0.0 libevent-pthreads-2.0-5 libevent-2.0-5 build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-thread-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev libzmq3-dev libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler libqrencode-dev python-pip

pip install construct==2.5.2 scrypt

# other

sudo apt-get update

sudo apt-get install build-essential gcc make perl dkms

reboot

sudo apt-get install git

sudo apt-get install build-essential libtool autotools-dev automake pkg-config libssl-dev libevent-dev bsdmainutils

sudo apt-get install libboost-system-dev libboost-filesystem-dev libboost-chrono-dev libboost-program-options-dev libboost-test-dev libboost-thread-dev

sudo apt-get install libboost-all-dev

sudo apt-get install software-properties-common

sudo add-apt-repository ppa:bitcoin/bitcoin

sudo apt-get update

sudo apt-get install libdb4.8-dev libdb4.8++-dev

sudo apt-get install libminiupnpc-dev

sudo apt-get install libzmq3-dev

sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler

sudo apt-get install libqt4-dev libprotobuf-dev protobuf-compiler

sudo apt-get install openssl1.0

sudo apt-get install libssl1.0-dev

sudo apt-get install libboost-program-options-dev

# QT Gui

sudo apt-get install libqt5gui5 libqt5core5a libqt5dbus5 qttools5-dev qttools5-dev-tools libprotobuf-dev protobuf-compiler

sudo apt-get install libqrencode-dev

sudo apt-get install libminiupnpc-dev

sudo apt-get install build-essential libtool autotools-dev automake pkg-config bsdmainutils python3

# Portainer

sudo docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock portainer/portainer

# Extra

for i in $(docker ps -q); do docker exec $i /focalpoint/src/focalpoint-cli  getblockchaininfo; done

for i in $(docker ps -q); do docker exec $i /focalpoint/src/focalpoint-cli  generate 2  & done

# Extra 2

Generate 11 blocks to myaddress:

bitcoin-cli generatetoaddress 11 "myaddress"

If you are using the Bitcoin Core wallet, you can get a new address to send the newly generated bitcoin to with::

bitcoin-cli getnewaddress

# Extra 3

wget https://gist.githubusercontent.com/darosior/a5d93c6245a32f7a8bed2ac4e33a0011/raw/89c49515febbd55ffb60e4add9d08f299862cde4/install_libdb4.8.sh && chmod +x install_libdb4.8.sh && ./install_libdb4.8.sh amd64 && rm install_libdb4.8.sh
as root

# Estra 4

/root/.focal/focal.conf
rpcuser=users
rpcpassword=qweryuiop

# Extra 5

wget http://download.oracle.com/berkeley-db/db-4.8.30.zip

unzip db-4.8.30.zip

cd db-4.8.30

cd build_unix/

../dist/configure --prefix=/usr/local --enable-cxx

make

make install

compile error fix

sed -i 's/__atomic_compare_exchange/__atomic_compare_exchange_db/g' db-4.8.30/dbinc/atomic.h

